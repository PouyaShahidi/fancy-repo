package controller

import (
	"fmt"
	"log"
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"

	"github.com/gorilla/mux"
)

type server struct {
	//  db     *someDatabase
	router  *mux.Router
	metrics *Metrics
}

func StartRouter() {

	r := mux.NewRouter()
	metrics := NewPrometheus()
	s := server{router: r, metrics: metrics}
	s.router.HandleFunc("/", s.metrics.MetricsHandler(s.handleIndex()))
	s.router.HandleFunc("/metrics", promhttp.Handler().ServeHTTP)
	s.router.NotFoundHandler = s.catchAllHandler()

	log.Fatal(http.ListenAndServe(":7070", r))
}

func (s *server) handleIndex() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "This is v0.2.1")
	}
}

func (s *server) catchAllHandler() http.Handler {

	return http.HandlerFunc(s.metrics.MetricsHandler(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(404)
	}))
}
