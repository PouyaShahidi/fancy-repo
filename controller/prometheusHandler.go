package controller

import (
	"net/http"
	"strconv"

	"github.com/prometheus/client_golang/prometheus"
)

type Metrics struct {
	OpsProcessed *prometheus.CounterVec
}

func NewPrometheus() *Metrics {

	counters := prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "handler_requests_total",
			Help: "The total number of processed events.",
		},
		[]string{"endpoint", "code", "method"})

	prometheus.MustRegister(counters)

	metrics := &Metrics{OpsProcessed: counters}

	return metrics

}

func (m *Metrics) MetricsHandler(handler http.HandlerFunc) http.HandlerFunc {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		rec := statusRecorder{w, 200}
		handler.ServeHTTP(&rec, r)
		m.OpsProcessed.With(prometheus.Labels{"endpoint": r.RequestURI, "code": strconv.Itoa(rec.code), "method": r.Method}).Inc()
	})
}

type statusRecorder struct {
	http.ResponseWriter
	code int
}

func (rec *statusRecorder) WriteHeader(code int) {
	rec.code = code
	rec.ResponseWriter.WriteHeader(code)
}
