package main

import (
	"log"

	"bitbucket.org/PouyaShahidi/fancy-repo/controller"
)

func main() {

	log.Printf("Server started")
	controller.StartRouter()
}
